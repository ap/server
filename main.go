package main

import (
    "git.0x0.st/ap/server/src/config"
    _"git.0x0.st/ap/server/src/database"
    _"git.0x0.st/ap/server/src/event"
    _"git.0x0.st/ap/server/src/script"
    _"git.0x0.st/ap/server/src/encryption"
    _"git.0x0.st/ap/server/src/web"
    "fmt"
    "log"
    "os"
)


/*
this file acts as a bootstrapper for the server.
load config, init stuff, start webrouter etc etc.
*/
func main() {
    cfg, err := config.LoadConfig(os.Getenv("HOME") + "/.ap/config.toml")
    if err!= nil {
        log.Fatal("failed to load config")
    }
    fmt.Println("Starting", cfg.Instance.Name, ", your personal", cfg.Instance.Software, "instance")
} 
